class Checkin < ActiveRecord::Base
  self.primary_key = 'business_id'

  belongs_to :business

  def count(opts = {})

    opts[:day] ||= Date.today.strftime("%A").to_sym
    opts[:period] ||= :morning

    checkin_info = JSON.parse(self.checkin_info)
    days_of_week = Date::DAYNAMES.map { |d| d.to_sym }
    day_index = days_of_week.index(opts[:day])
    


      count = case opts[:period]
        when :morning     then count_for_period(checkin_info, day_index, [6, 11])
        when :noon        then count_for_period(checkin_info, day_index, [12, 16])
        when :afternoon   then count_for_period(checkin_info, day_index, [17, 20])
        when :night       then count_for_period(checkin_info, day_index, [21, 24])
        else
          raise "Wrong period value"
      end

    count
  end

  private
    def count_for_period(checkin_info, day_index, range)
      count = 0

      checkin_info.each do |period, c|
        h, di = period.split('-')
        next if (di.to_i != day_index)
        count += c.to_i if h.to_i.between?(range[0], range[1])
      end

      count
    end
end
