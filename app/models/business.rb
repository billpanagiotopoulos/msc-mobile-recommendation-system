class Business < ActiveRecord::Base
  self.primary_key = :business_id

  has_many :category_businesses
  has_many :categories, :through => :category_businesses
  
  has_many :checkins

  geocoded_by :full_address

  attr_accessor :checkins_count

  def map_link
    "http://maps.google.com/?q=#{self.latitude},#{self.longitude}"
  end

  def self.sort_with_period_and_day(day, period, k, *near_args)
    businesses = near(*near_args)
    businesses.each do |b|
      count = 0
      if !b.checkins.nil?
        b.checkins.each do |c|
          current_count = c.count(:day => day, :period => period)
          count += current_count if !current_count.nil?
        end
      end

      b.checkins_count = count
    end

    businesses.sort { |x, y| y.checkins_count <=> x.checkins_count }.take(k)
  end
end
