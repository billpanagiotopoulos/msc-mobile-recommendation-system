require 'benchmark'

class BusinessesController < ApplicationController
  def index
    @categories = Category.sorted_categories
    @periods = [
                  ['Morning (6am - 11pm)', 'morning'], 
                  ['Noon (12pm - 16pm)', 'noon'], 
                  ['Afternoon (17pm - 20pm)', 'afternoon'], 
                  ['Night (9pm - 6am)', 'night']
                ]
    @locations = [
                    ['Place A', '35.095238,-80.779082'], 
                    ['Place B', '35.239174,-80.886314'], 
                    ['Place C', '33.508455,-112.05002'], 
                    ['Place D', '35.1997826,-80.8518372']
                  ]
  end

  def search
    @categories = Category.sorted_categories if search_params[:category_id].empty?
    @categories = [Category.find(search_params[:category_id])] unless search_params[:category_id].empty?
    @categories.map {|c| c.name}

    @location = search_params[:location].split(',').map {|x| x.to_f}
    @day_of_week = search_params[:day_of_week].to_sym
    @period = search_params[:period].downcase.to_sym
    @number_of_results = search_params[:number_of_results].to_i
    @radius = search_params[:radius].to_f


    @execution_time = Benchmark.realtime do

      @businesses = Business.sort_with_period_and_day(@day_of_week, 
                                                      @period, 
                                                      @number_of_results, 
                                                      @location, 
                                                      @radius, 
                                                      :units => :km)

      @businesses.select! {|b| (@categories & (b.categories)).size > 0}
    end
  end

  def search_params
    params.require(:search).permit([:category_id, :location, :radius, :period, :day_of_week, :number_of_results])
  end

  def form_result
	
	if (params[:category]['id'] != "")
		bus_cat_ids = CategoryBusiness.select('business_id').where("category_id = ?", params[:category]['id'])
		bus_cat  = Business.select('name').where('business_id': bus_cat_ids)
		bus_cat_names = bus_cat.map{ |b| b.name}
	end
	if ((params[:location] != "") && (params[:period] != ""))
		coords = params[:location].split(',')
		coord_1 = coords[0].gsub!('[','').to_f
		coord_2 = coords[1].gsub!(']','').to_f
		timestamp = params[:period].split(',')
		day = timestamp[0]
		period = timestamp[1].gsub!(' ','')
		integerno = timestamp[2].gsub!(' ','')
		bus_per = Business.select('name').sort_with_period_and_day(day, period, integerno.to_i, [coord_1,coord_2], 0.5, :units => :km)
		bus_per_names = bus_per.map{ |b| b.name}
	elsif ((params[:location] != "") && (params[:period] == ""))
		coords = params[:location].split(',')
		coord_1 = coords[0].gsub!('[','').to_f
		coord_2 = coords[1].gsub!(']','').to_f
		bus_loc = Business.select('name').near([coord_1,coord_2], 0.5, :units => :km)
		bus_loc_names = bus_loc.map{ |b| b.name}
	end
	if bus_cat_names && bus_per_names
		@final = bus_cat_names & bus_per_names
	elsif bus_cat_names && bus_loc_names
		@final = bus_cat_names & bus_loc_names
	elsif bus_cat_names
		@final = bus_cat_names
	elsif bus_per_names
		@final = bus_per_names
	elsif bus_loc_names
		@final = bus_loc_names	
	end
  end
end
