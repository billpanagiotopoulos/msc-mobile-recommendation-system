# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ -> 
	geoID = null

	initialize = ->
		$('#location_select').on 'change', ->
			$('#location_manual').prop('checked', true)
			navigator.geolocation.clearWatch(geoID)
			
			lat_long = $(this).val().split(',');
			if lat_long[0] == ''
				$('#location_title').text('Location')
				$('#search_location').val('')
			else
				long_lat_str = lat_long[0] + ',' + lat_long[1];
				$('#location_title').html('Location (' + long_lat_str + ') <a target="_blank" href="http://maps.google.com/?q=' + long_lat_str + '">show on map</a>')
				$('#search_location').val(long_lat_str)

		$('#period_select').on 'change', ->
			$('#period_manual').prop('checked', true)

			if $(this).val() == ''
				$('#period_title').text('Period')
				$('#search_period').val('')
			else
				$('#period_title').text('Period (' + $(this).val() + ')')
				$('#search_period').val($(this).val())

		$('#day_of_week_select').on 'change', ->
			$('#day_of_week_manual').prop('checked', true)

			if $(this).val() == ''
				$('#day_of_week_title').text('Day of Week')
				$('#search_day_of_week').val('')
			else
				$('#day_of_week_title').text('Day of Week (' + $(this).val() + ')')
				$('#search_day_of_week').val($(this).val())

		$('#location_auto').on 'click', ->
			$('#location_title').text('Location (getting location)')
			if (navigator.geolocation)
				geoID = navigator.geolocation.watchPosition (position)-> 
					long_lat_str = position.coords.latitude + ',' + position.coords.longitude
					$('#location_title').html('Location (' + long_lat_str + ') <a target="_blank" href="http://maps.google.com/?q=' + long_lat_str + '">show on map</a>')
					navigator.geolocation.clearWatch(geoID)
					$('#search_location').val(long_lat_str)

		$('#period_auto').on 'click', ->
			$('#period_title').text('Period (' + getPeriod() + ')')
			$('#search_period').val(getPeriod())

		$('#day_of_week_auto').on 'click', ->
			$('#day_of_week_title').text('Day of Week (' + getDayOfWeek() + ')')
			$('#search_day_of_week').val(getDayOfWeek())

		$('input[type="radio"]').on 'click', ->
			updateSubmitButton()

		$('select').on 'change', ->
			updateSubmitButton()

		getPeriod = () ->
			date = new Date()
			hours = date.getHours()
			if (hours >= 6 && hours <= 11)
				return 'Morning'
			else if (hours >= 12 && hours <= 16)
				return 'Noon'
			else if (hours >= 17 && hours <= 20)
				return 'Afternoon'
			else 
				return 'Night'

		getDayOfWeek = () ->
			date = new Date()
			return $('#day_of_week_select option').get(date.getDay()+1).text

		updateSubmitButton = () ->
			if $('input[type="radio"]:checked').length == 3
				$('#search').removeAttr('disabled');
			else
				$('#search').attr('disabled','disabled');

		$('input[type="radio"]').prop('checked', false)
		$('#search').attr('disabled','disabled');
		$('select').val($(this).find("option:first").val());

	$(document).on 'page:change', ->
		initialize()


