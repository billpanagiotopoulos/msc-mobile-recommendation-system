module BusinessesHelper
  def bootstrap_radio_button(id, name, label, checked = '')
    "<div class='radio'>
        <label>
          <input type='radio' name='#{name}' id='#{id}' #{checked}='#{checked}'>
          #{label}
        </label>
      </div>".html_safe
  end
end
