Applicaiton URL
=============
http://returnfalse.net:3001/

Project Setup
=============
```
1.  cd /path/to/mobile_recommendation_system
2.  bundle install
3.  rake db:setup
4.  rake yelp:setup
```

`yelp:setup` task may take a while (about 40-60 minutes) to complete

*Output of **yelp:setup*** 
```
-> Downloading dataset... done.
-> Inserting checkins to db... 45166 entries inserted!
-> Inserting businesses to db... 61184 entries inserted!
```
---------------------------------------
Reset Database
=============

Note: This will not download the dataset again if already downloaded

```
rake db:reset
rake yelp:setup
```
---------------------------------------
Models
=============

To test the models you have to open the rails console. Type the following command from your terminal
```
rails console
```
Business
--------
    
**Attributes**: `business_id` `name` `full_address` `city` `state` `stars` `latitude` `longitude` `hours`

* To see all businesses type `Business.all` in rails console
* To see the categories for a specific business (for example the first business in db) type
```
business = Business.all[0]
business.categories
```
Category
--------
**Attributes**: `name`

* To see all categories type `Category.all` in rails console
* To see the businesses for a specific category (for example the first category in db) type
```
category = Category.all[0]
category.businesses
```
Checkin
--------
**Attributes**: `business_id` `checkin_info`

* To see all checkins type `Checkin.all` in rails console
* To see all checkins for a specific business (for example the first business in db) type
```
business = Business.all[0]
business.checkins
```
---------------------------------------
Search for Businesses near a specific location (latitude, longitude)
=============

If you want to search all nearby businesses for the location 40.068829, -88.24974 and radius 0.5 km, use the following command
```
Business.near([40.068829, -88.24974], 0.5, :units => :km)
```
---------------------------------------
Search for Businesses near a specific location (latitude, longitude) and sort results using day, period of day and k number
=============

1st param: one of `:Sunday` `:Monday` `:Tuesday` `:Wednesday` `:Thursday` `:Friday` `:Saturday`

2nd param: one of `:morning` `:noon` `:afternoon` `:night`

3rd param: an integer number

Rest of params: see geocoder gem docs

```
Business.sort_with_period_and_day(:Thursday, :morning, 5, [40.068829, -88.24974], 0.5, :units => :km)
```