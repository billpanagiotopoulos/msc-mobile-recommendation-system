class CreateCheckins < ActiveRecord::Migration
  def change
    create_table :checkins, { :id => false, :primary_key => :business_id } do |t|
      t.string :business_id, index: true, foreign_key: true
      t.text :checkin_info

      t.timestamps null: false
    end
  end
end
