class CreateCategoryBusinesses < ActiveRecord::Migration
  def change
    create_table :category_businesses, { :id => false, :primary_key => [:category_id, :business_id] } do |t|
      t.string :category_id, index: true, foreign_key: true
      t.string :business_id, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
