class CreateBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses, { :id => false, :primary_key => :business_id } do |t|
      t.string :business_id, index: true, unique: true
      t.string :name
      t.string :full_address
      t.string :city
      t.string :state
      t.float :stars
      t.float :latitude, index: true
      t.float :longitude, index: true
      t.text :hours

      t.timestamps null: false
    end
  end
end
