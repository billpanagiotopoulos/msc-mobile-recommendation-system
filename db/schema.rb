# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150527201137) do

  create_table "businesses", id: false, force: :cascade do |t|
    t.string   "business_id"
    t.string   "name"
    t.string   "full_address"
    t.string   "city"
    t.string   "state"
    t.float    "stars"
    t.float    "latitude"
    t.float    "longitude"
    t.text     "hours"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "businesses", ["business_id"], name: "index_businesses_on_business_id"
  add_index "businesses", ["latitude"], name: "index_businesses_on_latitude"
  add_index "businesses", ["longitude"], name: "index_businesses_on_longitude"

  create_table "categories", force: :cascade do |t|
    t.string   "business_id"
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "categories", ["business_id"], name: "index_categories_on_business_id"

  create_table "category_businesses", id: false, force: :cascade do |t|
    t.string   "category_id"
    t.string   "business_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "category_businesses", ["business_id"], name: "index_category_businesses_on_business_id"
  add_index "category_businesses", ["category_id"], name: "index_category_businesses_on_category_id"

  create_table "checkins", force: :cascade do |t|
    t.string   "business_id"
    t.text     "checkin_info"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "checkins", ["business_id"], name: "index_checkins_on_business_id"

  create_table "reviews", force: :cascade do |t|
    t.string   "business_id"
    t.float    "stars"
    t.text     "text"
    t.datetime "date"
    t.integer  "votes"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "reviews", ["business_id"], name: "index_reviews_on_business_id"

end
