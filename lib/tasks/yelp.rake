require 'open-uri'
require 'zlib'
require 'rubygems/package'
require 'yajl'

namespace :yelp do
  desc "Downloads and parses the dataset from yelp contest"
  task :setup => :environment do
    DATASET_URL = "https://www.dropbox.com/s/r7bx4m0nbh2hx5t/yelp_dataset_challenge_academic_dataset.tgz?dl=0&raw=1"
    TGZ_PATH = Rails.root.join('tmp', 'yelp', 'dataset.tgz')
    JSON_FILES = {
                    'checkin': 'yelp_academic_dataset_checkin.json', 
                    'review': 'yelp_academic_dataset_review.json',
                    'business': 'yelp_academic_dataset_business.json',
                    'categories': 'yelp_academic_dataset_category.json',
                  }

    FileUtils.mkdir_p(Rails.root.join('tmp', 'yelp'))

    unless File.exist?(TGZ_PATH)
      print "-> Downloading dataset..."
      File.open(TGZ_PATH, "wb") do |temp_file|
        open(DATASET_URL, "rb") do |remote_file|
          temp_file.write(remote_file.read)
        end
      end
      puts " done."
    end
    
    tar_extract = Gem::Package::TarReader.new(Zlib::GzipReader.open(TGZ_PATH))
    tar_extract.rewind # The extract has to be rewinded after every iteration
    tar_extract.each do |entry|
      if entry.full_name =~ /#{JSON_FILES[:checkin]}$/
        print "-> Inserting checkins to db..."
        counter = 0
        parse_entry(entry) do |json_object|
          attributes = json_object.symbolize_keys!.slice(:business_id, :checkin_info)
          attributes[:checkin_info] = attributes[:checkin_info].inspect.gsub('=>', ':')

          Checkin.new(attributes).save
          counter += 1
        end
        puts " #{counter} entries inserted!"
      elsif entry.full_name =~ /#{JSON_FILES[:review]}$/
        # print "-> Inserting reviews to db..."
        # counter = 0
        # parse_entry(entry) do |json_object|
        #   attributes = json_object.symbolize_keys!.slice(:business_id, :stars, :text, :date, :votes)

        #   Review.new(attributes).save
        #   counter += 1
        # end
        # puts " #{counter} entries inserted!"
      elsif entry.full_name =~ /#{JSON_FILES[:business]}$/
        print "-> Inserting businesses to db..."
        counter = 0
        parse_entry(entry) do |json_object|
          attributes = json_object.symbolize_keys!.slice(:business_id, :name, :full_address, :city, :state, :latitude, :longitude, :stars, :hours)
          attributes[:hours] = attributes[:hours].inspect
          b = Business.new(attributes)
          b.save
          json_object[:categories].each do |category_name|

            category = Category.where(name: category_name)[0]
            category = Category.new(name: category_name) if category.nil?

            b.categories << category
          end
          
          counter += 1
        end
        puts " #{counter} entries inserted!"
      end
    end
    tar_extract.close
  end

  def parse_entry(entry, &block)
    if !entry.nil?
        json_file = Rails.root.join('tmp', 'yelp', File.basename(entry.full_name))
        File.open(json_file, "wb") do |temp_file|
          while !entry.eof?
            temp_file.write(entry.read(1024 * 1024 * 128))
          end
        end
        json = File.new(json_file, 'r')
        parser = Yajl::Parser.new
        hash = parser.parse(json) do |json_object|
          if (block)
            yield(json_object)
          end
        end

        File.delete(json_file)
      end
  end
end
