Rails.application.routes.draw do
  root 'businesses#index'  

  post 'search', :controller => :businesses, :action => :search
end
